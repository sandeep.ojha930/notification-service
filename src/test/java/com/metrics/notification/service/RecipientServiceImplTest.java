package com.metrics.notification.service;

import com.metrics.notification.service.recipient.RecipientService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class RecipientServiceImplTest {
    @Autowired
    RecipientService recipientService;
    @DisplayName("find by name!!")
    @Test
    void getByName() {
        Assertions.assertEquals("sandeep.ojha930", recipientService.
                findByAccountName("sandeep.ojha930").getAccountName());
    }
}
