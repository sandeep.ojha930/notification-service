package com.metrics.notification.controller;

import com.metrics.notification.domain.Recipient;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;

import java.net.URL;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RecipientControllerTest {
    private int port = 8380;
    @Autowired
    private TestRestTemplate testRestTemplate;
    @Test
    public void getAccountByName() throws Exception {
        ResponseEntity<Recipient> responseEntity = testRestTemplate.getForEntity(
                new URL("http://127.0.0.1:" + port + "/recipients/sandeep.ojha930").toString(), Recipient.class);
        Assertions.assertEquals("sandeep.ojha930",responseEntity.getBody().getAccountName());
    }
}
