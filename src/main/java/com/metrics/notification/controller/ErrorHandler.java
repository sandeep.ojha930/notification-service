package com.metrics.notification.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ErrorHandler {

	private final static Logger log = LoggerFactory.getLogger(ErrorHandler.class);
	@ExceptionHandler(IllegalArgumentException.class)
	//@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ResponseEntity<String> processValidationError(IllegalArgumentException e) {
		log.info("Returning HTTP 400 Bad Request", e.getMessage());
		return new ResponseEntity<>(e.getMessage(),HttpStatus.BAD_REQUEST);
	}
}
