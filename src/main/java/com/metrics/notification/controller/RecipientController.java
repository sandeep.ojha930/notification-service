package com.metrics.notification.controller;

import com.metrics.notification.domain.Recipient;
import com.metrics.notification.service.recipient.RecipientService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping("/recipients")
public class RecipientController {

    @Autowired
    private RecipientService recipientService;

    @GetMapping("/{accountName}")
    public ResponseEntity<Recipient> getCurrentNotificationsSettings(@PathVariable String accountName) {
        log.info("Fetch the recipients by account: " + accountName);
        return new ResponseEntity<>(recipientService.findByAccountName(accountName), HttpStatus.OK);
    }

    @PutMapping("/{accountName}")
    public ResponseEntity<Recipient> saveCurrentNotificationsSettings(@PathVariable String accountName, @Valid @RequestBody Recipient recipient) {
        Recipient object = null;
        HttpStatus httpStatus;
        try {
            object = recipientService.save(accountName, recipient);
            httpStatus = HttpStatus.ACCEPTED;
            log.info("Data is saved successfully for account: " + accountName);
        } catch (Exception e) {
            httpStatus = HttpStatus.BAD_REQUEST;
            log.error("An errored occurred in saveCurrentNotificationsSettings method: " + e.getMessage());
        }
        return new ResponseEntity<>(object, httpStatus);
    }
}
