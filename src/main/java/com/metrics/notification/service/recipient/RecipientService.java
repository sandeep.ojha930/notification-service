package com.metrics.notification.service.recipient;

import com.metrics.notification.domain.Recipient;
import com.metrics.notification.enums.NotificationType;

import java.util.List;

public interface RecipientService {

    Recipient findByAccountName(String accountName);

    List<Recipient> findReadyToNotify(NotificationType type);

    Recipient save(String accountName, Recipient recipient);

    void markNotified(NotificationType type, Recipient recipient);
}
