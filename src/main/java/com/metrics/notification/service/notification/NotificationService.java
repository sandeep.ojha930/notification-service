package com.metrics.notification.service.notification;

public interface NotificationService {

	void sendRemindNotifications();
}
