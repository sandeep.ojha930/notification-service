package com.metrics.notification.service.email;

import com.metrics.notification.domain.Recipient;
import com.metrics.notification.enums.NotificationType;

import javax.mail.MessagingException;
import java.io.IOException;

public interface EmailService {

    void send(NotificationType type, Recipient recipient, String attachment) throws MessagingException, IOException;

}
