package com.metrics.notification.service.kafka;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.JSONPObject;
import com.metrics.notification.domain.NotificationSettings;
import com.metrics.notification.domain.Recipient;
import com.metrics.notification.domain.User;
import com.metrics.notification.enums.Frequency;
import com.metrics.notification.enums.NotificationType;
import com.metrics.notification.service.recipient.RecipientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JsonParser;
import org.springframework.boot.json.JsonParserFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
public class Consume {
    private static final Logger LOGGER = LoggerFactory.getLogger(Consume.class);

    @Autowired
    RecipientService recipientService;

    @KafkaListener(topics = "users", groupId = "group_id")
    public void consume(String message) throws JsonProcessingException {
        LOGGER.info("Consume message:: "+message);
        User user = new ObjectMapper().readValue(message, User.class);
        Recipient recipient = new Recipient();
        recipient.setAccountName(user.getUsername());
        recipient.setEmail(user.getEmail());
        Map<NotificationType, NotificationSettings> scheduledNotifications = new HashMap<>();
        scheduledNotifications.put(NotificationType.REMIND,new NotificationSettings(true, Frequency.WEEKLY, new Date()));
        recipient.setScheduledNotifications(scheduledNotifications);
        recipientService.save(user.getUsername(), recipient);
    }

}
