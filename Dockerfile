FROM openjdk:8u111-jdk-alpine
VOLUME /tmp
ADD /build/libs/notification-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
EXPOSE 8080/tcp
EXPOSE 80/tcp
EXPOSE 8380/tcp
EXPOSE 8381/tcp
EXPOSE 8382/tcp
